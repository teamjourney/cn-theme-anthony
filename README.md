# cn-theme-child

## Local development
1. Ensure your local environment hostname matches the `localDevUrl` value in *gulpfile.babel.js*
1. From the root of the theme directory, install dependencies with `npm install`
1. Compile assets and launch the live reload server with `npm run start`