<?php
/**
 * Extend CTA widget with additional classes
 */
function cta_layouts( $variations ) {
    $variations['c-cta--lede'] = 'Lede CTA';

    return $variations;
}
add_filter('cn-core-cta-widget-layouts', 'cta_layouts');
