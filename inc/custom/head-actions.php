<?php

/**
 * Output additional scripts/styles or favicons
 *
 * Anything that belongs within the <head></head> elements
 */
function cn_custom_head_action() {
    echo "<link href='https://fonts.googleapis.com/css?family=Lato|PT+Serif&display=swap' rel='stylesheet'>";
}
add_action( 'cn/template/html_head', 'cn_custom_head_action', 10, 1 );