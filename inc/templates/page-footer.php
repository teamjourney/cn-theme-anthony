<?php

use CN\Core\Partial;

/**
 * Footer variables
 * 
 * Use to register the menu offcanvas
 * <?php Partial\cn_partial('offcanvas-menu') ?>
 */

?>

<footer class="c-page-footer">

    <div class="c-page-footer__promoted">
        <div class="c-promoted-footer">
            <nav class="c-nav-promoted c-nav-promoted--footer">
                <?php wp_nav_menu([
                    'theme_location' => 'nav_promoted',
                    'fallback_cb' => false,
                    'container' => 'c-nav-promoted',
                    'depth' => 1
                    ]) ?>
            </nav>
        </div>
    </div>
    
    <div class="c-page-footer__legal">
        <nav class="c-nav-footer">
            <?php wp_nav_menu([
                'theme_location' => 'nav_footer',
                'fallback_cb' => false,
                'container' => 'c-menu-footer',
                'depth' => 1
            ]) ?>
        </nav>
    </div>

    <small>
        <?php echo get_copyright_notice() ?>
        <?php if (is_front_page()) : ?>
            <a href="https://journey.travel">Website by Journey</a><span>('s newest developer)</span>
        <?php endif; ?>
    </small>
    
</footer>