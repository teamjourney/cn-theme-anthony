<?php

use CN\Core\Partial;

$header_button_text = get_field('header_button_text', 'option');

?>

<header class="c-page-header" role="banner" aria-labelledby="header-label">
    
    <span class="show-for-sr" id="header-label"><?php echo get_bloginfo('name') ?></span>
    <a href="#main-navigation" class="show-on-focus"><?php _e('Skip to primary navigation', 'cn-theme-child') ?></a>
    <a href="#main-content" class="show-on-focus"><?php _e('Skip to content', 'cn-theme-child') ?></a>

    <div class="c-page-header__nav-container">
        <div class="c-page-header__nav-container--inner">

            <div class="c-page-header__actions">
                <button
                    id="main-nav-toggle"
                    class="c-navicon c-navicon--spin"
                    aria-label="Menu"
                    role="button"
                    aria-controls="main-navigation"
                    data-module="class-toggle"
                    data-module-options='{"target": "body", "class": "navigation--is-active"}'
                >
                    <div class="c-navicon__box">
                        <div class="c-navicon__inner"></div>
                    </div>
                </button>
            </div>

            <div class="c-page-header__promoted">
                <div class="c-promoted-header">
                    <nav class="c-nav-promoted c-nav-promoted--header">
                        <?php wp_nav_menu([
                            'theme_location' => 'nav_promoted',
                            'fallback_cb' => false,
                            'container' => 'c-nav-promoted',
                            'depth' => 1
                        ]) ?>
                    </nav>
                </div>
            </div>

            <div class="c-page-header__logo">
                <a href="<?php echo home_url('/') ?>">
                    <?php include STYLESHEETPATH . '/assets/svg/logo.svg' ?>
                </a>
            </div>

            <div class="c-page-header__contact">
                <div class="c-contact-header">
                    <div class="c-contact-header__inner">
                        <button
                            class="c-contact-header__trigger"
                            data-module="class-toggle"
                            data-module-options='{"target": "body", "class": "contact--is-active"}'>
                            <?php echo $header_button_text ?><br />
                        </button>
                        <div class="c-contact-header__menu">
                            <nav class="c-nav-actions">
                                <?php wp_nav_menu([
                                    'theme_location' => 'nav_actions',
                                    'fallback_cb' => false,
                                    'container' => 'c-nav-actions',
                                    'depth' => 1
                                ]) ?>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</header>