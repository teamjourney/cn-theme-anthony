<?php
/**
 * Extend CTA widget with additional classes
 */
function cta_variations( $variations ) {
    $variations['c-cta--xyz'] = 'Variation';

    return $variations;
}
// add_filter('cn-core-cta-widget-variations', 'cta_variations');
