<?php

/**
 * Register child menu locations
 */
register_nav_menus( [
    'nav_promoted'   => 'Header Left Navigation',
    'nav_actions'  => 'Header Right Navigation'
] );