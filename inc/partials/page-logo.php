<?php

$page_logo_link = get_field('page_logo_link', 'options');

?>

<div class="c-page-logo">
    <a
        href="<?php echo $page_logo_link; ?>"
        class="c-page-logo__anchor"
    >
        <?php include STYLESHEETPATH . '/assets/svg/logo.svg' ?>
    </a>
</div>
