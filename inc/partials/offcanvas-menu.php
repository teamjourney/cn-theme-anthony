<?php

// $offcanvasDefaultImageId = get_field('offcanvas_default_image_id', 'options');

$menuLocations = get_nav_menu_locations();
$mainMenuId = $menuLocations['nav_main'];
$mainMenuItems = wp_get_nav_menu_items($mainMenuId);

?>

<section class="c-offcanvas c-offcanvas--menu">
    <div class="c-offcanvas__inner">
        <button
            class="c-offcanvas__close"
            data-module="class-toggle"
            data-module-options='{"target": "body", "class": "menu-offcanvas-is-visible"}'
        >
            <span class="show-for-sr"><?php _e('Close', 'cn-core-theme') ?></span>
        </button>

        <nav
            class="c-offcanvas__menu  c-nav-primary"
            role="navigation"
            aria-label="Site Navigation"
        >
            <?php wp_nav_menu([
                'theme_location' => 'nav_main',
                'fallback_cb' => false,
                'container' => 'c-nav-primary',
                'depth' => 2
            ]) ?>
        </nav>

        <!-- <footer class="c-offcanvas__footer">
            <p>
                <?php echo get_field('address', 'option') ?> |
                <?php echo get_field('phone', 'option') ?>
            </p>
        </footer> -->
    </div>
</section>