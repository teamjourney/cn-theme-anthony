<?php

/**
 * Add theme specific variations to rows
 */
function cn_child_row_variations( $variations ) {
    // $variations['c-CLASSNAME'] = 'Verbose class description';

    return $variations;
}
add_filter('cn_row_variations', 'cn_child_row_variations');