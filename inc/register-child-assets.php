<?php

function cn_child_enqueue_styles() {
    wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css');
    wp_enqueue_style('dist-style', get_stylesheet_directory_uri() . '/dist/css/style.css');

    wp_enqueue_script('dist-child-app', get_stylesheet_directory_uri() . '/dist/js/child-app.js', array('jquery'), '1.0', true);
}
add_action('wp_enqueue_scripts', 'cn_child_enqueue_styles');
