<?php
/**
 * Add theme specific layouts to rows
 */
function cn_row_vertical_alignment( $variations ) {
    $variations['c-row--valign-xyz'] = 'Layout';

    return $variations;
}
// add_filter('cn/row/vertical_alignment', 'cn_row_vertical_alignment');

function cn_row_spacing( $variations ) {
    $variations['c-row--margin-xyz'] = 'Row Spacing';

    return $variations;
}
// add_filter('cn/row/spacing', 'cn_row_spacing');

function cn_row_column_spacing( $variations ) {
    $variations['c-row--column-xyz'] = 'Column spacing';

    return $variations;
}
// add_filter('cn/row/column_spacing', 'cn_row_column_spacing');