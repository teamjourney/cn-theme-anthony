<?php
define('HOTELCMS_VERSION', '2.4');

include_once 'inc/register-child-assets.php';
include_once 'inc/register-child-navigation.php';
include_once 'inc/child-widget-layouts.php';
include_once 'inc/child-widget-variations.php';
include_once 'inc/child-row-layouts.php';
include_once 'inc/child-row-variations.php';


/**
 * Include custom functions
 */
foreach ( glob( dirname(__FILE__) . '/inc/custom/*.php') as $filename ) {

    include_once $filename;
}


/**
 * Include core functions
 */
if ( file_exists(TEMPLATEPATH . '/core-functions.php') ) {
    require_once TEMPLATEPATH . '/core-functions.php';
}
