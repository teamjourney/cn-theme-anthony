import $ from 'jquery'

class Sample {
    constructor() {
        this.elements = {
            body: $('body')
        }

        this.setBodyClass('has-sample-module')
    }

    setBodyClass(cssClass) {
        this.elements.body.addClass(cssClass)
    }
}

export default Sample