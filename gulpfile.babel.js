import { src, dest, watch, series, parallel } from 'gulp'
import sass from 'gulp-sass'
import postcss from 'gulp-postcss'
import sourcemaps from 'gulp-sourcemaps'
import autoprefixer from 'autoprefixer'
import del from 'del'
import webpack from 'webpack-stream'
import browserSync from 'browser-sync'
import notify from 'gulp-notify'
import imageMin from 'gulp-imagemin'

const localDevUrl = 'http://anthony-site.test'

const server = browserSync.create()

const sourcePaths = {
  sass: 'assets/scss/**/*.scss',
  php: '**/*.php',
  js: 'assets/js/**/*.js',
  svg: 'assets/svg/*.svg',
  img: 'assets/img/**/*',
  fonts: 'assets/fonts/*'
}

const childAppJs = 'child-app.js'

const appPaths = {
  sass: 'assets/scss/style.scss',
  js: `assets/js/${childAppJs}`
}

const outputPath = 'dist'

export const styles = () => {
  return src(appPaths.sass)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', (error) => {
      sass.logError
      return notify().write(error)
    }))
    .pipe(postcss([ autoprefixer ]))
    .pipe(sourcemaps.write(''))
    .pipe(dest(`${outputPath}/css`))
    .pipe(server.stream())
    .pipe(notify({ message: 'Sass compiled 😎' }))
}

export const scripts = () => {
  return src(appPaths.js)
  .pipe(webpack({
    module: {
      rules: [
        {
          test: /\.js$/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        }
      ]
    },
    mode: 'development',
    devtool: 'inline-source-map',
    output: {
      filename: childAppJs
    },
    externals: {
      jquery: 'jQuery'
    }
  }))
  .pipe(dest(`${outputPath}/js`))
}

export const images = () => {
  return src(sourcePaths.img)
  .pipe(imageMin())
  .pipe(dest(`${outputPath}/img`))
}

export const svgs = () => {
  return src(sourcePaths.svg)
  .pipe(dest(`${outputPath}/svg`))
}

export const fonts = () => {
  return src(sourcePaths.fonts)
  .pipe(dest(`${outputPath}/fonts`))
}

export const watchForChanges = () => {
  watch(sourcePaths.sass, styles)
  watch(sourcePaths.js, series(scripts, reload))
  watch(sourcePaths.php, reload)
  watch(sourcePaths.img, series(images, reload))
  watch(sourcePaths.svg, series(svgs, reload))
  watch(sourcePaths.fonts, series(fonts, reload))
}

export const clean = () => del([outputPath])

export const serve = done => {
  server.init({
    proxy: localDevUrl
  })
  done()
}

export const reload = done => {
  server.reload()
  done()
}

export const dev = series(clean, parallel(styles, scripts, svgs, images, fonts), serve, watchForChanges)
export default dev